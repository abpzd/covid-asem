module.exports = {
  root: true,

  extends: [
    './node_modules/vue-cli-plugin-md-base/eslintrc.js',
  ],

  settings: {
    'import/resolver': {
      webpack: {
        config: './webpack.config.js',
      },
    },
  },
};
