const regionStats = {
  id: null,
  region: '',
  cases: 0,
  new_cases: 0,
  deaths: 0,
  new_deaths: 0,
};

export default {
  global: {
    confirmed: 0,
    recovered: 0,
    serious: 0,
    deaths: 0,
  },
  usa: {
    confirmed: 0,
    recovered: 0,
    serious: 0,
    deaths: 0,
  },
  stats: [
    regionStats,
  ],
};
