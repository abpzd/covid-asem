export const newsItems = [
  {
    image: require('@/images/news/1.jpg'),
    title: 'Why grocery shelves won\'t be empty for long',
    to: 'https://www.bbc.com/worklife/article/20200401-covid-19-why-we-wont-run-out-of-food-during-coronavirus',
  },
  {
    image: require('@/images/news/2.jpg'),
    title: 'Who’s eligible for COVID-19 stimulus checks? Your questions, answered',
    to: 'https://www.cnbc.com/2020/04/01/whos-eligible-for-covid-19-stimulus-checks-your-questions-answered.html',
  },
  {
    image: require('@/images/news/3.jpg'),
    title: 'The Science Behind A 14-Day Quarantine After Possible COVID-19 Exposure',
    to: 'https://www.npr.org/sections/health-shots/2020/04/01/824903684/the-science-behind-a-14-day-quarantine-after-possible-covid-19-exposure',
  },
  {
    image: require('@/images/news/4.jpg'),
    title: 'The COVID-19 Prescription for Your Fears',
    to: 'https://thriveglobal.com/stories/the-covid-19-prescription-for-your-fears/',
  },
  {
    image: require('@/images/news/5.png'),
    title: 'The $94 billion fitness industry is reinventing itself as Covid-19 spreads',
    to: 'https://www.cnn.com/2020/04/01/business/fitness-studios-coronavirus/index.html',
  },
  {
    image: require('@/images/news/6.jpg'),
    title: 'How to boost global resilience to COVID-19',
    to: 'https://www.weforum.org/agenda/2020/04/how-to-boost-global-resilience-to-covid-19/',
  },
  {
    image: require('@/images/news/7.png'),
    title: 'This is what China did to beat coronavirus. Experts say America couldn\'t handle it',
    to: 'https://www.usatoday.com/story/news/world/2020/04/01/coronavirus-covid-19-china-radical-measures-lockdowns-mass-quarantines/2938374001/',
  },
  {
    image: require('@/images/news/8.jpg'),
    title: 'COVID-19 brings out America’s real-life heroes',
    to: 'https://chicago.suntimes.com/2020/4/1/21203156/covid-19-coronavirus-doctors-hospitals-trump-briefings-dakota-access-pipeline-letters',
  },
  {
    image: require('@/images/news/9.jpg'),
    title: 'One Millennial’s Guide to COVID-19: 8 Ways to Embrace Our New Normal',
    to: 'https://www.dailysignal.com/2020/04/01/one-millennials-guide-to-covid-19-8-ways-to-embrace-our-new-normal/',
  },
  {
    image: require('@/images/news/10.jpg'),
    title: 'The Pandemic Toolkit Parents Need',
    to: 'https://www.psychologytoday.com/us/blog/pulling-through/202004/the-pandemic-toolkit-parents-need',
  },
  {
    image: require('@/images/news/11.jpg'),
    title: 'Leading During the COVID-19 Crisis',
    to: 'https://thriveglobal.com/stories/leading-during-the-covid-19-crisis/',
  },
  {
    image: require('@/images/news/12.jpg'),
    title: 'Some Insurers Waive Patients\' Share Of Costs For COVID-19 Treatment',
    to: 'https://www.npr.org/sections/health-shots/2020/03/30/824075753/good-news-with-caveats-some-insurers-waive-costs-to-patients-for-covid-19-treatm',
  },
  {
    image: require('@/images/news/13.jpg'),
    title: 'CoVid-19: A Global Metamorphosis',
    to: 'https://thriveglobal.com/stories/covid-19-a-global-metamorphosis/',
  },
  {
    image: require('@/images/news/14.jpg'),
    title: 'Social distancing can’t last forever. Here’s what should come next.',
    to: 'https://www.vox.com/science-and-health/2020/3/26/21192211/coronavirus-covid-19-social-distancing-end',
  },
  {
    image: require('@/images/news/15.jpg'),
    title: 'Why It Takes So Long To Get Most COVID-19 Test Results',
    to: 'https://www.npr.org/sections/health-shots/2020/03/28/822869504/why-it-takes-so-long-to-get-most-covid-19-test-results',
  },
  {
    image: require('@/images/news/16.jpg'),
    title: 'Mindset Matters: Cracking The Code And Adjusting To The New Normal During The COVID-19 Epidemic',
    to: 'https://www.forbes.com/sites/jonathankaufman/2020/03/28/mindset-matters-cracking-the-code-and-adjusting-to-the-new-normal-during-the-covid-19-epidemic/#3859e8c6360a',
  },
  {
    image: require('@/images/news/17.jpg'),
    title: 'Navigating The New Normal: Addressing Employee Experience During COVID-19',
    to: 'https://www.forbes.com/sites/sap/2020/03/27/navigating-the-new-normal-addressing-employee-experience-during-covid-19/#5a388cdc1bdc',
  },
  {
    image: require('@/images/news/18.jpg'),
    title: 'It’s time to harness your superpowers to minimize the impact of Covid-19',
    to: 'https://thriveglobal.com/stories/its-time-to-harness-your-superpowers-to-minimise-the-impact-of-covid-19/',
  },
  {
    image: require('@/images/news/19.jpg'),
    title: 'CPAP Machines Were Seen As Ventilator Alternatives, But Could Spread COVID-19',
    to: 'https://www.npr.org/sections/health-shots/2020/03/27/822211604/cpap-machines-were-seen-as-ventilator-alternatives-but-could-spread-covid-19',
  },
  {
    image: require('@/images/news/20.jpeg'),
    title: 'Crowdsourcing to fight covid-19',
    to: 'https://www.economist.com/international/2020/03/26/crowdsourcing-to-fight-covid-19',
  },
  {
    image: require('@/images/news/21.jpg'),
    title: 'Why soap, sanitizer and warm water work against Covid-19 and other viruses',
    to: 'https://edition.cnn.com/2020/03/24/health/soap-warm-water-hand-sanitizer-coronavirus-wellness-scn/index.html',
  },
  {
    image: require('@/images/news/RCV.jpg'),
    title: 'What\'s in the $2 trillion Bill?',
    to: 'https://www.weforum.org/agenda/2020/03/senate-coronavirus-stimulus-covid19-united-states-health-economics/',
  },
  {
    image: require('@/images/news/Vaccine.jpg'),
    title: 'Covid-19 Treatment and Vaccine Tracker',
    to: 'https://milkeninstitute.org/sites/default/files/2020-03/Covid19%20Tracker%20NEW3-24-20-REVISED.pdf',
  },
  {
    image: require('@/images/news/PPE_masks.jpg'),
    title: 'Calling All People Who Sew And Make: You Can Help Make Masks For 2020 Healthcare Worker PPE Shortage',
    to: 'https://www.forbes.com/sites/tjmccue/2020/03/20/calling-all-people-who-sew-and-make-you-can-help-solve-2020-n95-type-mask-shortage/#75b98af4e41d',
  },
  {
    image: require('@/images/news/track_testing.jpg'),
    title: 'Track Testing Covid-19 around the World',
    to: 'https://ourworldindata.org/covid-testing',
  },
  {
    image: require('@/images/news/remote_work.jpg'),
    title: 'Remote Jobs, Now Hiring!',
    to: 'https://www.google.com/search?ei=YIZ8Xpq-Ko3RmwWNo4ugCA&q=remote+jobs&oq=remote+jobs&gs_l=psy-ab.3..0i273l2j0l8.2344.2854..3044...0.1..0.118.329.0j3......0....1..gws-wiz.......0i71.HSIrzV50918&ved=0ahUKEwia7sPL-bfoAhWN6KYKHY3RAoQQ4dUDCAs&uact=5',
  },
  {
    image: require('@/images/news/airline.jpg'),
    title: 'Affect of Covid on Airlines (stats)',
    to: 'https://www.visualcapitalist.com/global-flight-capacity-coronavirus/',
  },
  {
    image: require('@/images/news/fashion.jpg'),
    title: 'Fashion brands are making face masks, medical gowns for the coronavirus crisis',
    to: 'https://www.latimes.com/lifestyle/story/2020-03-24/fashion-brands-face-masks-medical-surgical-gowns-coronavirus',
  },
  {
    image: require('@/images/news/future_of_work.png'),
    title: 'How coronavirus COVID-19 is accelerating the future of work',
    to: 'https://www.zdnet.com/article/how-coronavirus-may-accelerate-the-future-of-work/',
  },
  {
    image: require('@/images/news/bigger_disruption.jpg'),
    // date: '23 september 2020',
    title: 'Get Ready, A Bigger Disruption Is Coming',
    to: 'https://www.bloomberg.com/opinion/articles/2020-03-16/coronavirus-foreshadow-s-bigger-disruptions-in-future',
  },
  {
    image: require('@/images/news/what_the_future_could.jpg'),
    // date: '23 september 2020',
    title: 'What could our future look like with novel coronavirus COVID-19?',
    to: 'https://www.clickorlando.com/health/2020/03/26/what-could-our-future-look-like-with-novel-coronavirus-covid-19/',
  },
  {
    image: require('@/images/news/in_the_midst.jpg'),
    // date: '23 september 2020',
    title: 'In the Midst of the Coronavirus Crisis, We Must Start Envisioning the Future Now',
    to: 'https://www.newyorker.com/news/our-columnists/in-the-midst-of-the-coronavirus-crisis-we-must-start-envisioning-the-future-now',
  },
  {
    image: require('@/images/news/palestinian.jpg'),
    // date: '23 september 2020',
    title: 'How a Palestinian shoemaker started the West Bank’s only mask factory overnight',
    to: 'https://www.972mag.com/coronavirus-mask-factory-hebron/',
  },
  {
    image: require('@/images/news/five_ways.jpg'),
    // date: '23 september 2020',
    title: 'Five ways coronavirus could shape our digital future',
    to: 'https://www.odi.org/blogs/16747-five-ways-coronavirus-could-shape-our-digital-future',
  },
  {
    image: require('@/images/news/what_mexico.jpg'),
    // date: '23 september 2020',
    title: 'What Mexico’s response to H1N1 can teach us about coronavirus and future pandemics',
    to: 'https://atlanticcouncil.org/blogs/new-atlanticist/what-mexicos-response-to-h1n1-can-teach-us-about-coronavirus-and-future-pandemics/',
  },
  {
    image: require('@/images/news/freelancer.jpg'),
    // date: '23 september 2020',
    title: 'A freelancer\'s guide to the coronavirus downturn: jobs, resources and support networks',
    to: 'https://www.thedrum.com/news/2020/03/23/freelancers-guide-the-coronavirus-downturn-jobs-resources-and-support-networks',
  },
  {
    image: require('@/images/news/chillng_paradox.jpg'),
    // date: '23 september 2020',
    title: 'Coronavirus gives us a terrifying glimpse of the future – and highlights a chilling paradox',
    to: 'https://www.theguardian.com/commentisfree/2020/mar/17/coronavirus-gives-us-a-terrifying-glimpse-of-the-future-and-highlights-a-chilling-paradox',
  },
  {
    image: require('@/images/news/immune.jpg'),
    // date: '23 september 2020',
    title: 'Coronavirus and Immune System',
    to: 'https://macrovegan.org/coronavirus-and-immune-system/',
  },
  {
    image: require('@/images/news/malware_phishing_scams.jpg'),
    // date: '23 september 2020',
    title: 'The Internet is drowning in COVID-19-related malware and phishing scams',
    to: 'https://arstechnica.com/information-technology/2020/03/the-internet-is-drowning-in-covid-19-related-malware-and-phishing-scams/',
  },
];
