export const CaseListItemMixin = {
  props: {
    item: {
      type: Object,
      required: true,
    },
  },
};
