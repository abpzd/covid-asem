export const shuffle = (a) => {
  const arrayLength = a.length;

  for (let i = arrayLength - 1; i > 0;) {
    const j = Math.floor(Math.random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]];
    i -= 1;
  }
  return a;
};
