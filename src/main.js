// import app styles
import '@/styles/app.scss';
// import node_modules styles
import 'swiper/css/swiper.css';

import numeral from 'numeral';

// import node_modules packages
import Vue from 'vue';
import capitalize from 'lodash/capitalize';
// import truncate from 'lodash/truncate';

import Autosize from 'vue-directives-piecedata/Autosize';

// directives
import Jump from 'vue-directives-piecedata/Jump';

// import app
import App from '@/App';
import router from '@/router';

Vue.config.productionTip = false;

// filters
Vue.filter('capitalize', value => capitalize(value));
Vue.filter('numberFormat', value => numeral(value)
  .format('0,0'));
Vue.filter('pluralize', (value, word) => value === 1 ? word : `${word}s`);
Vue.filter('title', (value) => {
  const words = value.split('_');

  for (let i = 0; i < words.length; i += 1) {
    words[i] = capitalize(words[i]);
  }

  return words.join(' ');
});

// directives
Vue.directive('autosize', Autosize);
Vue.directive('jump', Jump);

// components

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App),
});
