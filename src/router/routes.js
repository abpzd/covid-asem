import AppLayout from '@/components/layouts/AppLayout';

import HomeIndex from '@/components/home/HomeIndex';
import PromoVideoIndex from '@/components/PromoVideoIndex';
import AboutIndex from '@/components/about/AboutIndex';
import CommunityIndex from '@/components/CommunityIndex';
import NumbersIndex from '@/components/numbers/NumbersIndex';
import NewsIndex from '@/components/news/NewsIndex';
import DataSourcesIndex from '@/components/DataSourcesIndex';
import NotFoundErrorIndex from '@/components/errors/NotFoundErrorIndex';

const routes = [
  {
    path: '/',
    component: AppLayout,
    children: [
      {
        path: '/',
        name: 'home',
        component: HomeIndex,
        meta: {
          headerInverted: true,
        },
      },
      {
        path: '/video',
        name: 'video',
        component: PromoVideoIndex,
        meta: {
          headerHidden: true,
        },
      },
      {
        path: '/about',
        name: 'about',
        component: AboutIndex,
      },
      {
        path: '/community',
        name: 'community',
        component: CommunityIndex,
      },
      {
        path: '/numbers',
        name: 'numbers',
        component: NumbersIndex,
      },
      {
        path: '/news',
        name: 'news',
        component: NewsIndex,
      },
      {
        path: '/data-sources',
        name: 'data-sources',
        component: DataSourcesIndex,
      },
    ],
  },

  {
    path: '*',
    name: 'error',
    component: NotFoundErrorIndex,
  },
];

export default routes;
