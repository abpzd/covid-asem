const manifest = require('./public/manifest.json');

module.exports = {
  pwa: {
    name: manifest.name,
    themeColor: manifest.theme_color,
    msTileColor: manifest.theme_color,
  },

  transpileDependencies: [
    'vue-directives-piecedata',
  ],
};
