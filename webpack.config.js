const path = require('path');

module.exports = {
  resolve: {
    extensions: [
      '.js',
      '.jsx',
      '.mjs',
      '.ts',
      '.tsx',
      '.vue',
    ],
    alias: {
      '@': path.resolve(__dirname, './src'),
    },
  },
};
